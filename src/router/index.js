import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import ListCommands from '../views/ListCommands'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/listMatches',
    name: 'listMatches',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/ListMatches.vue')
  },
  {
    path: '/listCommands',
    name: 'listCommands',
    component: ListCommands
  },
  {
    path: '/aboutNatch/:id',
    name: 'AboutMatch',
    props: true,
    component: () => import('../views/AboutMatch.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
